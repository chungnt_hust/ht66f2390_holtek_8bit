/*
 * systemClock.c
 *
 *  Created on: Oct 20, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "systemClock.h"


/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
#define WDTC_REG _wdtc


/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */



/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
#define SCC_REG _scc
#define HIRCC_REG _hircc


/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
void SystemClock_config(scs_t cks, hfcs_t fhs, lfcs_t fss, fhiden_t fhiden, fsiden_t fsiden)
{
    scc_reg_t sccReg;
    sccReg.cks = cks;
    sccReg.fhs = fhs;
    sccReg.fss = fss;
    sccReg.fhiden = fhiden;
    sccReg.fsiden = fsiden;

    SCC_REG = sccReg;
}

uint8_t SystemClock_set(hircfs_t hircfs, hircen_t hircen)
{
    hircc_reg_t hirccReg;
    hirccReg.hircfs = hircfs;
    hirccReg.hircen = hircen;

    HIRCC_REG = hirccReg;
    while(!_hircf);	
    return 1;
}
