/*
 * systemClock.h
 *
 *  Created on: Oct 20, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 * 
          Type              | Name |   Frequency |   Pins
External High Speed Crystal | HXT  | 400kHz~16MHz| OSC1/OSC2
Internal High Speed RC      | HIRC | 8/12/16MHz  |   �
External Low Speed Crystal  | LXT  | 32.768kHz   |  XT1/XT2
Internal Low Speed RC       | LIRC |   32kHz     |     �
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef SYSTEMCLOCK_H
#define SYSTEMCLOCK_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include <HT66F2390.h>
#include <stdint.h>

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
/*  SCC Register */
typedef enum
{
    CKS_DIV1 = 0,
    CKS_DIV4,
    CKS_DIV8,
    CKS_DIV16,
    CKS_DIV32,
    CKS_DIV64,
    CKS_SUB,
} scs_t; // System clock selection

typedef enum
{
    FHS_HIRC = 0,
    FHS_HXT,
} hfcs_t; // High Frequency clock selection

typedef enum
{
    FSS_LIRC = 0,
    FSS_LXT,
} lfcs_t; //  Low Frequency clock selection

typedef enum
{
    FHIDEN_DIS = 0,
    FHIDEN_EN,
} fhiden_t; //   High Frequency oscillator control when CPU is switched of

typedef enum
{
    FSIDEN_DIS = 0,
    FSIDEN_EN,
} fsiden_t; //   Low Frequency oscillator control when CPU is switched of

typedef struct
{
    uint8_t fsiden:1;
    uint8_t fhiden:1;
    uint8_t fss:1;
    uint8_t fhs:1;
    uint8_t dum:1;
    uint8_t cks:3;
} scc_reg_t;

/*  HIRCC Register */
typedef enum
{
    HIRC_8 = 0,
    HIRC_12,
    HIRC_16,
} hircfs_t; //   HIRC frequency selection

typedef enum
{
    HIRC_DIS = 0,
    HIRC_EN,
} hircen_t; //    HIRC oscillator enable control

typedef struct
{
    uint8_t hircen:1;
    uint8_t hircf:1;
    uint8_t hircfs:2;
} hircc_reg_t;
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */





/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void SystemClock_config(scs_t cks, hfcs_t fhs, lfcs_t fss, fhiden_t fhiden, fsiden_t fsiden);
uint8_t SystemClock_set(hircfs_t hircfs, hircen_t hircen);
#endif
#ifdef __cplusplus
}
#endif
