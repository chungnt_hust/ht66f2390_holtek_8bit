/*
 * watchdog.h
 *
 *  Created on: Oct 20, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef WATCHDOG_H
#define WATCHDOG_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include <HT66F2390.h>
#include <stdint.h>

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
#define DIS_WDT              0b10101000 // disable watchdog
#define EN_WDT_TIMEOUT_MODE1 0b01010000 // enable watchdog, timeout period = 2^8/fLIRC
#define EN_WDT_TIMEOUT_MODE2 0b01010001 // enable watchdog, timeout period = 2^10/fLIRC
#define EN_WDT_TIMEOUT_MODE3 0b01010010 // enable watchdog, timeout period = 2^12/fLIRC
#define EN_WDT_TIMEOUT_MODE4 0b01010011 // enable watchdog, timeout period = 2^14/fLIRC
#define EN_WDT_TIMEOUT_MODE5 0b01010100 // enable watchdog, timeout period = 2^15/fLIRC
#define EN_WDT_TIMEOUT_MODE6 0b01010101 // enable watchdog, timeout period = 2^16/fLIRC
#define EN_WDT_TIMEOUT_MODE7 0b01010110 // enable watchdog, timeout period = 2^17/fLIRC
#define EN_WDT_TIMEOUT_MODE8 0b01010111 // enable watchdog, timeout period = 2^18/fLIRC
#define RESET_MCU            0x00       // disable watchdog

/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */





/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void Watchdog_set(uint8_t dat);

#endif
#ifdef __cplusplus
}
#endif
