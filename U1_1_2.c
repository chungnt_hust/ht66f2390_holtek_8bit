// PROGRAM	: U1_1_2.c								2017.0413
// FUNCTION	: LED Scanning Demo Program (Built-in)	By Steven
#include <HT66F2390.h>
#include "MyType.h"			
//#define LED_Port  _ph								
//#define LED_PortC _phc
void Delayms(u16);									//??????						
void main()
{	_wdtc=0b10101111;								// turn off watchdog
	//LED_PortC=0x0;									//?? LED_Port ?????
	//LED_Port=0xFE;									//?? LED_Port ??
	_phc0 = 0;
	_phc1 = 0;
	while(1)
	{	//while(LED_Port & 0b10000000)				//?MSB??0?????
		//{	Delayms(100);
		//	GCC_RL(LED_Port);						//??
		//}
		//while(LED_Port & 0b00000001)				//?LSB??0?????	
		//{	Delayms(200);
		//	GCC_RR(LED_Port);						//??
		//}				
		_ph0 = 1;
		_ph1 = 1;
		Delayms(1000);
		_ph0 = 0;
		_ph1 = 0;
		Delayms(1000);
	}
}
void Delayms(u16 del)								//??del*200????
{	u16 i;											//@fSYS=8MH,??del*1ms
	for(i=0;i<del;i++) GCC_DELAY(2000);
}	